<?php

namespace Drupal\cert\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cert.
 *
 * @ingroup cert
 */
class CertDeleteForm extends ContentEntityDeleteForm {


}
