<?php

namespace Drupal\cert\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\cert\Entity\Cert;

/**
 * Cert queue worker.
 *
 * @QueueWorker(
 *   id = "cert_queue_worker",
 *   title = @Translation("Cert queue worker"),
 *   cron = {"time" = 60}
 * )
 */
class CertQueueWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($entity_id) {
    /** @var \Drupal\cert\Entity\Cert $entity */
    $entity = Cert::load($entity_id);
    if ($entity === NULL) {
      return;
    }
    /** @var \Drupal\cert\Synchronizer $synchronizer */
    $synchronizer = \Drupal::service('cert.synchronizer');
    try {
      $synchronizer->synchronize($entity_id);
    }
    catch (\Exception $e) {
      \Drupal::logger('cert')->alert($e->getMessage());
    }
  }

}
