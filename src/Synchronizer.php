<?php

namespace Drupal\cert;

use Drupal\cert\Entity\Cert;
use Spatie\SslCertificate\SslCertificate;

/**
 * The Synchronizer class.
 */
class Synchronizer {

  /**
   * The cert entity.
   *
   * @var \Drupal\cert\Entity\Cert
   */
  private $cert;

  /**
   * The certificate.
   *
   * @var \Spatie\SslCertificate\SslCertificate
   */
  private $certificate;

  /**
   * Indicates whether the issue needs update.
   *
   * @var bool
   */
  private $updated = FALSE;

  /**
   * Whether allow to call the Issue::save() method.
   *
   * @var bool
   */
  private $allowSaving;

  /**
   * Downloads data and starts assigning values.
   *
   * @param \Drupal\issue\Entity\Issue|string $entity
   *   The issue entity ID.
   * @param bool $allow_saving
   *   Whether allow to call Issue::save().
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function synchronize($entity, $allow_saving = TRUE) {
    $this->allowSaving = $allow_saving;

    if ($entity instanceof Cert) {
      $this->cert = $entity;
    }
    else {
      $this->cert = Cert::load($entity);
    }

    if ($this->cert === NULL) {
      return;
    }

    $host = $this->cert->label();
    $certificate = SslCertificate::createForHostName($host);
    if ($certificate === FALSE) {
      return;
    }

    $this->certificate = $certificate;
    $this->doSynchronize();
  }

  /**
   * Does save values.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function doSynchronize() {
    $this->cert->setValid($this->certificate->isValid())
      ->setIssuer($this->certificate->getIssuer())
      ->setIssued($this->certificate->validFromDate()->getTimestamp())
      ->setExpired($this->certificate->expirationDate()->getTimestamp())
      ->setLastChecked(time());
    $this->save();
  }

  /**
   * Saves the issue entity updates.
   *
   * @return $this
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function save() {
    if ($this->allowSaving) {
      $this->cert->save();
    }
    return $this;
  }

}
