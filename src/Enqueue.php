<?php

namespace Drupal\cert;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;

/**
 * The Enqueue class.
 */
class Enqueue {

  const CERT_QUEUE_WORKER = 'cert_queue_worker';
  const STATE_KEY_CERT_LAST_CHECK = 'cert_last_check';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;


  /**
   * The queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  private $queue;

  /**
   * The settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $settings;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private $state;

  /**
   * Enqueue constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueueFactory $queue_factory, ConfigFactory $config_factory, StateInterface $state) {
    $this->entityTypeManager = $entity_type_manager;
    $this->queue = $queue_factory->get(self::CERT_QUEUE_WORKER);
    $this->settings = $config_factory->get('cert.settings');
    $this->state = $state;
  }

  /**
   * Issue ID generator.
   */
  private function certIds() {
    $query = $this->entityTypeManager->getStorage('cert')->getQuery();
    $result = $query->accessCheck(FALSE)->execute();
    foreach ($result as $entity_id) {
      yield $entity_id;
    }
  }

  /**
   * Enqueue cert ids.
   */
  public function enqueue() {
    $interval = $this->settings->get('interval');
    $current = time();
    $last_enqueue = $this->state->get(self::STATE_KEY_CERT_LAST_CHECK, $current);
    if ($last_enqueue === $current || $current > $last_enqueue + $interval) {
      foreach ($this->certIds() as $entity_id) {
        $this->queue->createItem($entity_id);
      }
      $this->state->set(self::STATE_KEY_CERT_LAST_CHECK, time());
    }
  }

}
