<?php

namespace Drupal\cert\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cert.
 *
 * @ingroup cert
 */
interface CertInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Cert name.
   *
   * @return string
   *   Name of the Cert.
   */
  public function getName();

  /**
   * Sets the Cert name.
   *
   * @param string $name
   *   The Cert name.
   *
   * @return \Drupal\cert\Entity\CertInterface
   *   The called Cert.
   */
  public function setName($name);

  /**
   * Gets the Cert creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cert.
   */
  public function getCreatedTime();

  /**
   * Sets the Cert creation timestamp.
   *
   * @param int $timestamp
   *   The Cert creation timestamp.
   *
   * @return \Drupal\cert\Entity\CertInterface
   *   The called Cert.
   */
  public function setCreatedTime($timestamp);

}
