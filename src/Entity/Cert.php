<?php

namespace Drupal\cert\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Cert.
 *
 * @ingroup cert
 *
 * @ContentEntityType(
 *   id = "cert",
 *   label = @Translation("Cert"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\cert\Form\CertForm",
 *       "add" = "Drupal\cert\Form\CertForm",
 *       "edit" = "Drupal\cert\Form\CertForm",
 *       "delete" = "Drupal\cert\Form\CertDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\cert\CertHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\cert\CertAccessControlHandler",
 *   },
 *   base_table = "cert",
 *   translatable = FALSE,
 *   admin_permission = "administer cert",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "host",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/cert/{cert}",
 *     "add-form" = "/cert/add",
 *     "edit-form" = "/cert/{cert}/edit",
 *     "delete-form" = "/cert/{cert}/delete",
 *   },
 *   field_ui_base_route = "cert.settings"
 * )
 */
class Cert extends ContentEntityBase implements EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * Gets host name.
   */
  public function host() {
    return $this->get('host')->value;
  }

  /**
   * Sets the host name.
   *
   * @param string $host
   *   The host name.
   *
   * @return $this
   */
  public function setHost($host) {
    $this->set('host', $host);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * Gets the issuer.
   *
   * @return string
   *   The issuer.
   */
  public function issuer() {
    return $this->get('issuer')->value;
  }

  /**
   * Sets the issuer.
   *
   * @param string $issuer
   *   The issuer to set.
   *
   * @return $this
   */
  public function setIssuer($issuer) {
    $this->set('issuer', $issuer);
    return $this;
  }

  /**
   * Gets the valid.
   */
  public function valid() {
    return $this->get('valid')->value;
  }

  /**
   * Sets valid.
   *
   * @param bool $valid
   *   Valid or not.
   *
   * @return $this
   */
  public function setValid($valid) {
    $this->set('valid', $valid);
    return $this;
  }

  /**
   * Gets the issued date timestamp.
   *
   * @return int
   *   The issued timestamp.
   */
  public function issued() {
    return $this->get('issued')->value;
  }

  /**
   * Sets the issued timestamp.
   *
   * @param int $issued
   *   The issued timestamp.
   *
   * @return $this
   */
  public function setIssued($issued) {
    $this->set('issued', $issued);
    return $this;
  }

  /**
   * Gets the expired date timestamp.
   *
   * @return int
   *   The expired timestamp.
   */
  public function expired() {
    return $this->get('expired')->value;
  }

  /**
   * Sets the expired timestamp.
   *
   * @param int $expired
   *   The expired timestamp.
   *
   * @return $this
   */
  public function setExpired($expired) {
    $this->set('expired', $expired);
    return $this;
  }

  /**
   * Gets the last checked date timestamp.
   *
   * @return int
   *   The expired timestamp.
   */
  public function lastChecked() {
    return $this->get('last_checked')->value;
  }

  /**
   * Sets the last checked timestamp.
   *
   * @param int $last_checked
   *   The last checked timestamp.
   *
   * @return $this
   */
  public function setLastChecked($last_checked) {
    $this->set('last_checked', $last_checked);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The author of the cert.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['host'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Host name'))
      ->setDescription(t('The host name to monitor.'))
      ->setSettings([
        'max_length' => 1023,
        'text_processing' => 0,
      ])
      ->addConstraint('UniqueField')
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['issuer'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Issuer'))
      ->setSettings([
        'max_length' => 127,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['valid'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Valid'))
      ->setDescription(t('The status of the cert, invalid (FALSE) valid (TRUE).'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'boolean',
        'weight' => 1,
        'settings' => [
          'format' => 'unicode-yes-no',
          'format_custom_true' => '',
          'format_custom_false' => '',
        ],
      ])
      ->setRequired(TRUE);

    $fields['issued'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Issued'))
      ->setDescription(t('The date that cert was issued.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp_ago',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['expired'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Expiration date'))
      ->setDescription(t('The time that the issue was updated.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp_ago',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['last_checked'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last checked'))
      ->setDescription(t('The last time that the issue was checked.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp_ago',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    /** @var \Drupal\cert\Synchronizer $synchronizer */
    $synchronizer = \Drupal::service('cert.synchronizer');
    try {
      $synchronizer->synchronize($this, FALSE);
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }

  }

}
